package com.snapown.snapowngcm;

/**
 * Created by comp3 on 4/18/2016.
 */
public class Config {

    // CONSTANTS
    static final String SERVER_URL =  "http://app1.vdoit.club/gcm_server_files/register.php";
    // YOUR_SERVER_URL : Server url where you have placed your server files
    // Google project id
    static final String GOOGLE_SENDER_ID = "870214247555";  // Place here your Google project id

    /**
     * Tag used on log messages.
     */
    static final String TAG = "SnapOwnGcm";

    static final String DISPLAY_MESSAGE_ACTION =
            "com.snapown.snapowngcm.DISPLAY_MESSAGE";

    static final String EXTRA_MESSAGE = "message";





}
